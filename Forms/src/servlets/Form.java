package servlets;

import java.io.*;



import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet("/filling")



public class Form extends HttpServlet{
	
	public static String path(){
		return "C://users/111/Desktop/forms.csv";
	}
	
	public static void write(String fileName, String text) {
	    //���������� ����
	    File file = new File(fileName);
	 
	    try {
	        //���������, ��� ���� ���� �� ���������� �� ������� ���
	        if(!file.exists()){
	            file.createNewFile();
	        }
	 
	        //PrintWriter ��������� ����������� ������ � ����
	        PrintWriter out = new PrintWriter(file.getAbsoluteFile());
	        String s=read(path())+"\n"+ text;
	        try {
	            //���������� ����� � ����
	            out.print(s);
	        } finally {
	            //����� ���� �� ������ ������� ����
	            //����� ���� �� ���������
	            out.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }
	}
	
	public static String read(String fileName) throws FileNotFoundException {
	    //���� ����. ������ ��� ���������� ������
	    StringBuilder sb = new StringBuilder();
	 
	    File file=new File(fileName);
	    
	 
	    try {
	        //������ ��� ������ ����� � �����
	        BufferedReader in = new BufferedReader(new FileReader( file.getAbsoluteFile()));
	        try {
	            //� ����� ��������� ��������� ����
	            String s;
	            while ((s = in.readLine()) != null) {
	                sb.append(s);
	                sb.append("\n");
	            }
	        } finally {
	            //����� �� �������� ������� ����
	            in.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }
	 
	    //���������� ���������� ����� � �����
	    return sb.toString();
	}
	
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    PrintWriter out = resp.getWriter();
    out.println(getPageCode(""));
  }
  
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String name = req.getParameter("name");
    String password= req.getParameter("password");
    String sex=req.getParameter("sex");
    String delivery=req.getParameter("delivery");
    String path=path();
    String greetings;
    if(name.isEmpty() || password.isEmpty() || sex==null){
    	greetings="<b>Form is emty</b>";
    }else{
    	if(delivery==null)delivery="0";
    	
//    	File data=new File("Forms/WebContent/data.txt");
    	String s=read(path);
    	s=s+name+" "+password+" "+sex+" "+delivery;
//    	((DataOutput) data).writeUTF(s);
    	greetings=s+"<a href=http://localhost:8080/Forms/cards>Users cards</a>";
    	write(path, s);
    } 
    	PrintWriter out = resp.getWriter();
    	out.println(getPageCode(greetings));
//    	out.println("<b>"+read("C://users/111/Desktop/forms.txt")+"</b>");
    
  }
  
  protected String getPageCode(String content){
    return "<!DOCTYPE html><html>"
            + "<head><meta charset='UTF-8'><title>Form page</title></head>"
            + "<body >"
            + "<form action='' method='POST' >Mail: <input type='text' name='name'><br>Password: <input type='password' name='password'><br>Sex: <input type='radio' name='sex' value='1'>M<input type='radio' name='sex' value='0'>W<br>Mailing  list: <input type='checkbox' name='delivery' value='1'><br><input type='submit' value='send'></form>"
            + content
            + "</body></html>";
  }
}